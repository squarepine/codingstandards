# Documentation

Though your code should be relatively self-documenting, at least document all of your complex functions. Jumping into a well documented application codebase and library is helpful and appreciated, especially when the flow isn't obvious!

## System

Use [Doxygen](https://www.stack.nl/~dimitri/doxygen/manual/commands.html) for header documentation.

For implementation documentation, use the basic language comments to explain a complex or non-obvious piece of code.

## Style

Use two asterisks to delineate the beginning of a class, function, method or variable comment.

## Rules

* Only one space must follow the `/**`.
* The initial sentence must be a summary of the intent, or a well described intent in and of itself.
* This summary must be written in normal sentence case and end with a period.
* The summary must be written on the same line as the `/**`.
* If the comment is easy to read on one line, put it on one line!
* Otherwise, if the comment spans multiple lines, the closing `*/` must be on its own line.
* Use simple newlines for separator lines.
* Do not use a symbol to represent the beginning of a new line!
* Use the `@` symbol for commands.

```c++
/** Does stuff and things.
 
    This function is very important for... reasons.
*/
void foo();
```

```c++
/** A double-precision floating point representation of pi.
 
    @see https://en.wikipedia.org/wiki/Pi
*/
static const double pi = 3.141592654;
```

```c++
/** @see https://www.google.ca */
void bar();
```

## Command Rules

Do not redundantly specify default commands.

For example, do this to automatically make use of [`@brief`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdbrief) and [`@details`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmddetails):
```c++
/** Does stuff and things
 
    Does an important bunch of stuff and things.
*/
void foo();
```

Do not specify the following commands, unless there's something legal telling you otherwise (use your best judgement!):
* [`@author`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdauthor)
* [`@version`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdversion)
* [`@date`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmddate)
* [`@pre`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdpre)
* [`@copyright`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdcopyright)

Only use these commands in your code:
* [`@internal`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdinternal)
  * Only when overriding a method.
* [`@code`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdcode) and [`@endcode`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdendcode) to show examples.
* [`@param`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdparam) to describe parameters.
* [`@param[out]`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdparam) to describe outputted parameters.
  * For consistency, do **not** use [`@param[in]`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdparam) to describe inputted parameters.
  * Not using `@param[in]` helps the eye more easily catch the out of the ordinary `@param[out]` specifier.
* [`@return`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdreturn) or [`@returns`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdreturns) to describe the purpose behind the returned value or object.
* [`@see`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdsee)
  * To point to other pieces code, like methods or variables.
  * To point to web URLs.
* [`@bug`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdbug)
* [`@warning`](https://www.stack.nl/~dimitri/doxygen/manual/commands.html#cmdwarning)
