# Includes & Imports

## Context-Driven Inclusions

When including a new file, use the fewest and best file(s) for the context.

In C++, always use the C++ equivalent of the header. That is, unless you're in a tiny C context in a C++ codebase.

```c++
#include <cmath>

extern "C"
{
    //Obviously contrived as you should be using <cstdio>, but this is just an example.
    #include <stdio.h>
}
```

## Sorting Rules

In a header, sort the includes alphabetically, with project headers first, library headers next, and standard headers last.

In an implementation file, sort the includes alphabetically, with the representative header first, and the rest following suit with the header rules.
