# SquarePine Beerware License

The crew at SquarePine wrote all code and/or assets in this repository.

As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy us a beer in return.

Cheers,
- SquarePine

---

You might consider following us on social media:
* https://www.facebook.com/squarepine
* https://twitter.com/squarepine
