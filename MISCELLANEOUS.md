# Miscellaneous

## Compiler Specific `#pragma` Commands

It's easy - avoid these ones listed below. The point of development is to make your previous efforts and productions work easily anywhere... and some compilers just can't cope with the massive clutter and noise you create that are specific to your toolchain!

* Don't use `#pragma mark`
* Don't use `#pragma region`
