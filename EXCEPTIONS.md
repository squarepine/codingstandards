# Exceptions

## Don't Use Exceptions, No Exceptions

Exceptions make code overall really difficult to follow, especially library code.

The worst thing about them is that they risk leaving objects and data in bad states, typically because you or another developer forgot to account for something.

In practice, especially in C++ code, it makes refactoring exceptionally difficult and bug prone.

It's overall better to avoid this evil feature altogether.
