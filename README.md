# SquarePine's Coding Standards - README

Welcome to the place that describes our coding standard in detail, with the intent of highlighting all macroscopic and microscopic components to arrangement and placement of code and text. This is a guideline that is strictly adhered to in our repositories.

The coding standard is C/C++ biased, but is written in the hopes being generalised enough to apply to any C-influenced language (e.g.: Java, Objective-C).

Because of the time wasted following (and being confused by and/or clashing between) multiple standards, this coding standard is made to apply as consistently as possible to a variety of languages. The idea is that following similarities takes much less thought, enabling you to produce code with fewer distractions.

Ranging from best practices to just getting the whitespace and language sorted out, this should serve as a proper rail to roll on for your C++ projects.

Although we supply our coding standards with a free license, we would love it if you could let us know that you're using our standards!

### Tip: Getting Started

On your first read of the coding standards, it is suggested to get the overall feel of the style by going through the following sections firstly, in the order listed.
* [General Whitespace](https://gitlab.com/squarepine/codingstandards/blob/master/GENERAL_WHITESPACE.md)
* [Language](https://gitlab.com/squarepine/codingstandards/blob/master/LANGUAGE_AND_SPELLING.md)
* [Documentation](https://gitlab.com/squarepine/codingstandards/blob/master/DOCUMENTATION.md)

## What Does This Not Do?

We try to err on the philosophy that a coding standard applies to layout and formatting. There are exceptions sprinkled around so don't panic!

For code design you should read up on best practices and generally follow the project's conventions.

That being said, your code should prioritise the following:
* Simplicity
* Usability
* Maintainability
* Dependability
* Cross-platform compatibility

Note that efficiency is explicitly not part of the list. Only make changes for efficiency after measuring the performance with [A/B Testing](https://en.wikipedia.org/wiki/A/B_testing) (prove it!). Focus on not sacrificing anything from the list above, or at least do it in a tasteful and scoped way that makes it well communicated.

## Some Best Practices

Outside of the scope of this coding standard are some concepts better described by external sources.
* [Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)
* [KISS](https://en.wikipedia.org/wiki/KISS_principle)
* [Proudly Found Elsewhere](https://www.oddhill.se/blog/proudly-found-elsewhere)
  * Opposite of [Not Invented Here](https://www.webopedia.com/TERM/N/not_invented_here_syndrome.html)

## Influences

* [JUCE Coding Standards](https://www.juce.com/learn/coding-standards)
* [ISO CPP Core Guidelines](https://github.com/isocpp/CppCoreGuidelines)
* [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)
