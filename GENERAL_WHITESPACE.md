# General Whitespace

## Word Wrapping

Disable this feature in your editor.

### Reasoning

* Aim for predictable text positioning.
* Formatting is inconsistent when using word wrapping.
* Dynamic line breaking is distracting while you're typing.
* You make it difficult for others to read your code if they do not use that feature.
* Aim to have all of the precious text you spend your valuable time on looking the exact same, no matter where you bring it.

## Tabs vs Spaces

It's simple: no tabs. Instead, use 4 spaces.

### Reasoning

To allow for this standard's particular formatting and placement styles, and enforcing consistency across all text editors and IDEs.

## Newline/Line Ending

Configure all of your systems to use CRLF. This is to be absolutely sure your code will compile with Windows compilers. Most (if not all) other compilers are compatible with it. Don't forget that git has some settings to help cope with it.

### Changing Line Endings in Popular IDEs
* [Xcode](http://help.apple.com/xcode/mac/8.0/#/dev8669bf801)
* [Visual Studio](https://stackoverflow.com/questions/3802406/configure-visual-studio-with-unix-end-of-lines)
* [Eclipse](https://stackoverflow.com/questions/5003830/automatic-eol-conversion-in-eclipse)
* [NetBeans](http://plugins.netbeans.org/plugin/46829/change-line-endings-on-save)

## Column Limitation

There is no limit. Simply prefer keeping code about a half-window's space in an HD resolution view to allow viewing code side-by-side in tabs.

If a group of lines are too long, it may indicate a higher-order breakdown; splitting the code into multiple lines, multiple functions, etc...

## Braces

[Allman style](https://en.wikipedia.org/wiki/Indentation_style#Allman_style), where all curly braces are always on a separate line.

```c++
static int getBestFoo()
{
    if (somethingImportantHappened())
    {
        static int foo = 0;
        ++foo;
        return foo;
    }

    return -1;
}
```

```c++
static const char* daysOfTheWeek[] =
{
    NEEDS_TRANS ("Sunday"),
    NEEDS_TRANS ("Monday"),
    NEEDS_TRANS ("Tuesday")
    //[...]
};
```

```c++
do
{
}
while (someCondition);
```

## Functions and Methods

When writing a function or method:
* Write the name on the same line as the return type.
* Insert only one single space after the return type.
* Insert only one single space after an explicit calling convention specifier.

```c++
void __stdcall foo()
{
}
```

If you want to distinguish a group of parameters, or if the line is too long, break multiple parameters up into a coherent set thereof.

Also, always line up the first parameter's type on the next line with the previous line's first type.

```c++
void foo (int firstX, int firstY,
          int secondX, int secondY)
{
    //Do stuff with these parameters
}

void bar (ComplexType& a,
          OtherType& b,
          Something& c)
{
    //Do stuff with these parameters
}
```

## Templates

To start, note that the function and method parameter rules apply the same to template parameters.

Never insert a space before or after a template declaration's chevrons (ie: `<>`).

```c++
template<typename Type>
void foo (Type a)
{
    //Achieve something grand with 'a'
}
```

Place templates on the line before a function, method, `class` or `struct` declaration.

```c++
template<typename Type, typename OtherType>
void foo (Type a, OtherType otherA,
          Type b, OtherType otherB)
{
    //Do stuff with these parameters
}
```

## Casting

### C-style

Insert 1 space after the cast's `)`:

```c++
auto result = (double) value;
```

### C++ style

Follow the parentheses rules as you would with methods/functions and templates:

```c++
auto result = static_cast<double> (value);
```
