## Naming Conventions

Function names for `bool` are to start with a third-person indicative of the word _be_:

```c++
class Foo
{
public:
    bool isRunning() const noexcept;
    bool wasDoingSomethingImportant() const noexcept;
};
```

Boolean variables are to favour the same rules above, but in the event of a clashing name the variable must simply be a verb.

```c++
class Foo
{
public:
    Foo() noexcept { }

    bool isRunning() const noexcept { return running; }
    bool hasCompletedAllActions() const noexcept { return hasCompletedFirstAction && hasCompletedSecondAction; }

private:
    bool running = false;
    bool hasCompletedFirstAction = false, hasCompletedSecondAction = false;
};
```

## General Comparisons

Don't compare directly to true or false as this is implicit knowledge.

```c++
if (foo())
    //Do stuff
```

Use the !-operator to check for false booleans. Insert 1 space after the exclamation point.

```c++
if (! bar())
    //Do stuff
```
