# Language & Spelling

## Spelling
Comments and code must be written using [Canadian English (en-ca)](https://en.wikipedia.org/wiki/Canadian_English), favouring the British side of spelling for consistency's sake.

### Other Sources

* [TERMIUM Plus®](https://www.btb.termiumplus.gc.ca/tcdnstyl-chap?lang=eng&lettr=chapsect3&info0=3.01)
* [McGill](https://www.cs.mcgill.ca/~rwest/wikispeedia/wpcd/wp/c/Canadian_English.htm)

### Suffixes
* Use `-our` as in `colour`, instead of `color`.
* Use `-ise` as in `vectorise`, instead of `vectorize`.
* Applies to all variations of `-ise`, such as `-ising`, `-isable`, `-ises` and `-isation`.
* Use `-tre` as in `centre`, instead of `center`.

### Other Miscellaneous Word Examples
* Use `travelling` instead of `traveling`.
* Use `grey` instead of `gray`.

## General Wording Rules

Nothing is more irritating than reading code that has the tone of a crappy 80s computer, where the coder tried to jam as much functionality without naming anything worth a damn. This is still a prevalent way of thinking still today; by mathematicians who fancy software development, but especially by people who lack the ability to empathise with readers.

Write code that is without abbreviations, but more importantly understandable and legible - not just by you, but by others too.

Go the whole way to make names descriptive and effortless to digest, and definitely use comments if you think it will help!

### Whitespace is Not on the Endangered List

Don't try to conserve amount of code for the sake of file size or length limit - there are no such limits!

### Punch Cards are on the Endangered List

They also have limited space for comments, but we aren't using punch cards so don't worry about it.

## Encoding
For all compilers, old and new, comments and code must be written in ASCII. There is no guarantee that a compiler will work fine with just any code page - some even throw warnings! (e.g.: [Microsoft Visual Studio C4819](https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4819)).

### Reasoning
You definitely don't want to pick up source code and have to port it to an old compiler, only to waste time fixing encoding (how boring!).

Your time is better spent developing features and fixing bugs!

## General Formatting
Variables, functions and enumerator definitions are to be `camelCase`. Aside from macros, all other identifiers are to be formatted in `PascalCase`.

```c++
#define MACROS_ARE_ALL_CAPS 1

enum MyEnumeration
{
    valueA,
    valueB
};

class Foo
{
public:
    virtual int getBar() const
    {
        static const int bar = 4;
        return bar;
    }
};
```
